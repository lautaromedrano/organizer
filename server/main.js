import { Meteor } from 'meteor/meteor';
import { ItemCollection } from '/imports/api/ItemCollection';

const insertItem = (itemTitle) => ItemCollection.insert({ title: itemTitle });

Meteor.startup(() => {
  if (ItemCollection.find().count() === 0) {
    [
      'Do the Tutorial',
      'Follow the Guide',
      'Read the Docs',
      'Discussions'
    ].forEach(insertItem);
  }
});
