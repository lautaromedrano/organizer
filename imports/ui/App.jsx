import React from 'react';
import { useTracker } from 'meteor/react-meteor-data';

import { ItemCollection } from '/imports/api/ItemCollection';
import { Item } from './Item';
import { ItemForm } from './ItemForm';

const toggleChecked = ({ _id, isChecked }) => {
  ItemCollection.update(_id, {
    $set: {
      isChecked: !isChecked
    }
  });
};

const deleteItem = ({ _id }) => ItemCollection.remove(_id);

export const App = () => {
  const items = useTracker(() => ItemCollection.find({}, { sort: { createdAt: -1 }}).fetch());

  return (
    <div className="app">
      <header>
        <div className="app-bar">
          <div className="app-header">
            <h1>Task list</h1>
          </div>
        </div>
      </header>
      <div className="main">
        <ItemForm />

        <ul className="tasks">
          {
            items.map(item => <Item key={item._id} item={item} onCheckboxClick={toggleChecked} onDeleteClick={deleteItem} />)
          }
        </ul>
      </div>
    </div>
  );
};
