import React from 'react';

export const Item = ({ item, onCheckboxClick, onDeleteClick }) => {
    return (
        <li>
            <input type="checkbox" checked={!!item.isChecked} onClick={() => onCheckboxClick(item)} readonly />
            <span>{item.title}</span>
            <button onClick={ () => onDeleteClick(item)}>&times;</button>
        </li>
    );
};