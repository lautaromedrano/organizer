import React, { useState } from 'react';
import { ItemCollection } from '../api/ItemCollection';

export const ItemForm = () => {
    const [text, setText] = useState("");

    const handleSubmit = e => {
        e.preventDefault();

        if (!text) {
            return;
        }

        ItemCollection.insert({
            title: text.trim(),
            createdAt: new Date()
        });

        setText("");
    };

    return (
        <form className="task-form" onSubmit={handleSubmit}>
            <input type="text" placeholder="Type to add new items" value={text} onChange={(e) => setText(e.target.value)} />
            <button type="submit">Add Item</button>
        </form>
    );
};
